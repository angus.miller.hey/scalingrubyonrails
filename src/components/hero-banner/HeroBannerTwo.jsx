import React from "react";
import Brand from "../brand/Brand";

const HeroBannerTwo = () => {
  const handleSubmit = (event) => {
    event.preventDefault();
  };
  return (
    <div className="hero-banner-two">
      <div className="container">
        <div className="row align-items-start justify-content-between">
          <div className="col-lg-6 order-lg-last">
            <div className="illustration-holder">
              <img
                src="images/assets/cartman.jpeg"
                alt="shape"
                className="illustration_01"
              />
            </div>
          </div>

          <div className="col-xl-5 col-lg-6 order-lg-first">
            <div className="hero-text-wrapper md-mt-50">
              <h1>
                <span>
                  Meet the first <br></br>
                  <img
                    src="images/shape/line-shape-1.svg"
                    alt="shape"
                    className="cs-screen"
                  />
                </span>
                ANTI WOKE dev house!
              </h1>
              <p className="sub-text">
                We get shit down our way so you dont have to...
                Leaving you more time for meetings...
              </p>
              <form onClick={handleSubmit}>
                <input type="email" placeholder="Enter your email" />
                <button>Get Started</button>
              </form>
              <ul className="list-item d-flex mt-20">
                <li>Live chat</li>
                <li>Ticketing</li>
                <li>14 days free trial</li>
              </ul>
            </div>
            {/* /.hero-text-wrapper */}
          </div>
        </div>
        {/* /.row */}
      </div>
      {/* /.container */}

      <div className="mt-65 md-mt-50 trusted-companies">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-12">
              <p>
                Over <span>13,000+ Client</span> all over the world.
              </p>
            </div>
            <div className="col-12">
              <div className="companies-logo-slider">
                <Brand />
              </div>
            </div>
          </div>
          {/* End .row */}
        </div>
      </div>
      {/*  /.trusted-companies */}
    </div>
    // End .hero-banner-two
  );
};

export default HeroBannerTwo;
